/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AuthService } from '../auth/auth.service';
import { Router }           from '@angular/router';

import { LoginComponent } from './login.component';

class AuthServiceMock {
  public loggedIn(){
  }
}


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      providers: [
        {
          provide : AuthService,
          useClass: AuthServiceMock
        },{
          provide : Router,
          useClass:  class { navigate = jasmine.createSpy("navigate"); }
        }
      ],
      declarations: [ LoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should de created', () => {
    expect(component).toBeTruthy();
  });

  it('should init variable correctly', () => {
    expect(component.auth).toBeDefined();
    expect(component.auth.username).toBe('');
    expect(component.auth.password).toBe('');
    expect(component.displayError).toBeDefined();
    expect(component.displayError).toBeFalsy();
  });

  it('should set username and password',() => {
    // todo set username and password
  });

  it('should display error',() => {

  });

  it('should redirect to default route',() => {

  });
});
