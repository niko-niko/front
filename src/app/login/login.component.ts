import { Component, OnInit } from '@angular/core';
import { Router }           from '@angular/router';

import { Auth } from '../auth/auth';
import { AuthService } from '../auth/auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
	private auth : Auth;
	private displayError : boolean;

	constructor(private authService: AuthService,  private router:Router) { }

	ngOnInit() {
  		this.auth = new Auth('','');
  		this.displayError = false;
	}

	private onSubmit():void{
		this.authService.login(this.auth).subscribe((data) => {
			this.router.navigate(['']);
		},
		(err) => {
			this.displayError = true;
		});
	}

}
