import { Component, OnInit } from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
	private showNavbar: boolean = false;
	constructor(private router:Router){}

	ngOnInit(){
		this.router.events.subscribe((navInfo : any)=>{
			if(navInfo instanceof RoutesRecognized){
				this.showNavbar = navInfo.urlAfterRedirects !== '/login'
			}
		});
	}
}
