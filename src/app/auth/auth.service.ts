import { Injectable } from '@angular/core';
import { Http, Response } 	  from '@angular/http'
import { tokenNotExpired } from 'angular2-jwt';
import { Observable }       from 'rxjs/Observable';
import { Router }           from '@angular/router';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import 'rxjs/add/observable/throw';

import { Auth } from './auth';

const API_URL: string = 'http://localhost:3001/api';

@Injectable()
export class AuthService {

  	constructor(private http:Http,private router:Router) { }

  	public loggedIn() : boolean {
  		return tokenNotExpired();
	}

	public login(auth: Auth) : Observable<any> {
		return this.http.post(API_URL+'/users/login',{
			login : auth.username,
			password : auth.password
		}).map(this.extractData)
		.catch(this.handleError);
	}

	public logout(): void{
		localStorage.clear();
		this.router.navigate(['login']);
	}

	private extractData(response : Response){
		let token = response.json();
		localStorage.setItem('id_token',token);
		return token;
	}

	private handleError (error: Response | any) {
	  	// In a real world app, we might use a remote logging infrastructure
	  	let errMsg: string;
	  	if (error instanceof Response) {
	    	errMsg = `${error.status} - ${error.statusText || ''} ${error}`;
	  	} else {
	    	errMsg = error.message ? error.message : error.toString();
	  	}
	  	console.error(errMsg);
	  	return Observable.throw(error);
	}

}
