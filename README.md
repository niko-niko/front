# Niko Niko

A niko niko is an agile tool used for manage a project 

## Requirement

[Angular-cli](https://cli.angular.io/) installed 


## Install the project

```
npm i 

npm start
```
ou 
```
yarn install

yarn start
```

## License

This project is licensed under the GNU License - see the [LICENSE.md](LICENSE.md) file for details
